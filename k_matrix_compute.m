function [KELTOTAL] = k_matrix_compute(xx, yy, nel, gP, C, h)

% ======================================================================
%
% Local stifness matrix KELTOTAL
%
%    output: 
%      KELTOTAL - local stifness matrix
%                  
%    input:
%      xx, yy   - coordinates of elements nodes                         
%      nel - number of elements
%      gP - coordinates of Gauss integration points
%      C - constitutive matrix
%      h - thickness
%
% =========================================================================
     
    KELTOTAL=cell(8,8); [KELTOTAL{:,:}]=deal(sparse(nel,1));

    for intx=1:4
        xi = gP(intx,1);eta = gP(intx,2);
        ib = 3*intx-2;
        [B, detJ ] = b_matrix_compute(xi, eta, xx, yy, nel); 
        E=cell(3,8); [E{:,:}]=deal(zeros(nel,1));                          

        for i=1:3
            for j=1:8
                E{i,j}=C(i,1)*B{1,j}+C(i,2)*B{2,j}+C(i,3)*B{3,j};
            end
        end
        
        for i=1:8
            for j=1:i-1
                Kij = B{1,i}.*E{1,j}+B{2,i}.*E{2,j}+B{3,i}.*E{3,j};
                KELTOTAL{i,j} = KELTOTAL{i,j} + h*detJ.*Kij;
            end
            Kii = B{1,i}.*E{1,i}+B{2,i}.*E{2,i}+B{3,i}.*E{3,i};
            KELTOTAL{i,i} = KELTOTAL{i,i} + h*detJ.*Kii;
        end
    end  
end