function [s_zero] = stresses_zero(elements, xx, yy, u, nel, C)

% ======================================================================
%
% Calculation of stresses from integration point xi=0, eta=0 of each
% element to nodes
%
%    output: 
%      s_zero - values of stresses at nodes
%                  
%    input:
%      elements - element nodes numbers                         
%      xx, yy - coordinates of elements nodes
%      u - displacements of elements nodes
%      nel - number of elements
%      C - constitutive matrix
%
% =========================================================================

    e11 = zeros(nel,1);e22 = zeros(nel,1);e12 = zeros(nel,1);
    %
    [B] = b_matrix_compute(0, 0, xx, yy, nel);
    %
    for i=1:3
        for j=1:8                    
             E{i,j}=C(i,1)*B{1,j}+C(i,2)*B{2,j}+C(i,3)*B{3,j};
        end
    end
    for i=1:8                              
        e11 = e11 + B{1,i}.*u(:,i);
        e22 = e22 + B{2,i}.*u(:,i);
        e12 = e12 + B{3,i}.*u(:,i);
    end
    s_zero = C*[e11 e22 e12]';
    s_zero = [node_values(s_zero(1,:), elements, nel);
              node_values(s_zero(2,:), elements, nel);
              node_values(s_zero(3,:), elements, nel)];
%     el_values = ones(4,1)*e11';
%     node_values = sparse(i_n, j_n(:), el_values(:));
%     s11_nodes = node_values./node_nel;
end