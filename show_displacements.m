function show_displacements(U, elements, xx, yy, nel, nnodes)

% ======================================================================
%
% Calculation of traction forces
%               
%    input:
%      U - displacements of nodes                        
%      elements - element nodes numbers 
%      xx, yy - coordinates of elements nodes
%      nel - number of elements
%      nnodes - number of nodes
%
% =========================================================================

    UX = U(1:2:nnodes*2) ;
    UY = U(2:2:nnodes*2) ;
    disp('The maximum displacement UX'); disp(max(abs(UX)));
    disp('The maximum displacement UY'); disp(max(abs(UY)));
    
    scf=800; %scaling factor
    
    uxx = reshape(UX(elements(:,:),1),nel,4)*scf;
    uyy = reshape(UY(elements(:,:),1),nel,4)*scf;
    
    f1 = figure ;
    set(f1,'name','Mesh','numbertitle','off') ;
    
    patch(xx', yy','blue');
    hold on;
    patch(xx' + uxx', yy' + uyy','red');
    pbaspect([2.5 1 1]);
end