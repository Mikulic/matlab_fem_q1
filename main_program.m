% =========================================================================
%
%  This is program for plain stress problem with isoparametric 
%  quadilateral element. 
%
% ======================================================================
%
clc; clear;

%
% mesh imported from GMSH
%  
strip_mesh_1;

coord = msh.POS(:, 1:2);
elem = msh.QUADS(:, 1:4);

tic

%
% constitutive matrix
%  
[C] = c_matrix_compute();

%
% thickness
%
h=5;

%
% number of elem, nodes and degrees of freedom(dof)
%
nel=size(elem,1); nnodes=size(coord,1); ndof=2*nnodes;

%
% coordinates of element nodes
%

xx=reshape(coord(elem(:,:),1),nel,4);
yy=reshape(coord(elem(:,:),2),nel,4);

%
% Gauss points
%

gP = [-1,-1; 1,-1; 1,1; -1,1]*sqrt(3)/3;

%
% Local stifness matrix
%

[KELTOTAL] = k_matrix_compute(xx, yy, nel, gP, C, h);

%
% Global stifness matrix
%

[KGTOTAL] = kg_matrix_compute(KELTOTAL, elem, ndof);

%
% Assembling of the vector of traction forces at x=100
%  

P = 4000/40;       % Load
traction_force = [P, 0] ;
tnodes = find(coord(:,1)==100);

forces = traction_forces(traction_force, coord, tnodes);

%
% Dirichlet conditions
% 

dirichlet_nodesx0 = find(coord(:,1)==0);
dirichlet_nodesy0 = find(coord(:,2)==0);
dirichlet_dof = [2*dirichlet_nodesx0(:,1)-1; 2*dirichlet_nodesy0(:,1)];

%
% Solving
% 

U=zeros(ndof,1);
dof=1:ndof;
free_dof=setdiff(dof,dirichlet_dof);
U(free_dof)=KGTOTAL(free_dof,free_dof)\forces(free_dof);

toc

%
% Displacements
%

it1=elem(:,1); it2=elem(:,2); it3=elem(:,3); it4=elem(:,4);
u1=U(1:2:end); u2=U(2:2:end); 
% displacement of element coord
u=[u1(it1) u2(it1) u1(it2) u2(it2) u1(it3) u2(it3) u1(it4) u2(it4)];

show_displacements(U, elem, xx, yy, nel, nnodes);

%
% Stresses and strains at xi=0 eta=0
% 

[s_zero] = stresses_zero(elem, xx, yy, u, nel, C);

%
% Stresses extrapolation
% 

[s_extra] = stresses_extrapolation(elem, xx, yy, u, nel, C, gP);

%
% Plot stres s_11
% 

plot_value(s_extra(1,:)', coord, elem);

%
% Display max stress
% 

disp('S_ZERO_11');disp(max(abs(full(s_zero(1,:)))));
disp('S_EXTRA_11');disp(max(s_extra(1,:)));

%
% Display number of coord and elem
% 

disp('NODES');disp(nnodes);
disp('ELEMS');disp(nel);% 
