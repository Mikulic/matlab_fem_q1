function [node_values] = node_values(values, elements, nel)

% ======================================================================
%
% Calculation of node values from element values
%
%    output: 
%      node_values - values oat nodes 
%                  
%    input:
%      values  - values at element nodes
%      elements   - element nodes numbers                           
%      nel - number of elements
%
% =========================================================================

    % nuber of elements around each node
    node_ones = ones(4,nel);
    i_n = ones(4,nel);
    j_n = elements';
    node_nel = sparse(i_n(:), j_n(:), node_ones(:));
    %
    if length(values(:)) == nel
        values = ones(4,1)*values;
    end
    %
    node_values = sparse(i_n, j_n(:), values(:));
    node_values = node_values./node_nel;
end