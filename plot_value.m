function plot_value(value, nodes, elements)

% ======================================================================
%
% Node values plot
%            
%    input:
%      value  - values at nodes
%      nodes - nodes coordinetes
%      elements - element nodes numbers                           
%
% =========================================================================

    f2 = figure ;
    set(f2,'name','Stress','numbertitle','off') ;
    patch('vertices', nodes, 'faces', elements, 'facevertexcdata', value, 'facecolor', 'interp', 'LineStyle','none');
    colormap(jet); colorbar;
    pbaspect([2.5 1 1]);
end