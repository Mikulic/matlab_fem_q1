function [s_extra] = stresses_extrapolation(elements, xx, yy, u, nel, C, gP)

% ======================================================================
%
% Extrapolation of stresses from integration points of each
% element to nodes
%
%    output: 
%      s_extra - values of stresses at nodes
%                  
%    input:
%      elements - element nodes numbers                        
%      xx, yy - coordinates of elements nodes
%      u - 
%      nel - number of elements
%      C - constitutive matrix
%      gP - coordinates of Gauss integration points
%
% =========================================================================

    e411 = zeros(nel,4);e422 = zeros(nel,4);e412 = zeros(nel,4); 
    %
    expol = [ 1+sqrt(3)/2 -1/2 1-sqrt(3)/2 -1/2;
              -1/2 1+sqrt(3)/2 -1/2 1-sqrt(3)/2;
              1-sqrt(3)/2 -1/2 1+sqrt(3)/2 -1/2;
              -1/2 1-sqrt(3)/2 -1/2 1+sqrt(3)/2;];
    %
    for itx=1:4; xi = gP(itx,1); eta = gP(itx,2);
        [B] = b_matrix_compute(xi, eta, xx, yy, nel);
        for i=1:8                              
            e411(:,itx) = e411(:,itx) + B{1,i}.*u(:,i);
            e422(:,itx) = e422(:,itx) + B{2,i}.*u(:,i);
            e412(:,itx) = e412(:,itx) + B{3,i}.*u(:,i);
        end
    end
    %
    e411expol = e411*expol;
    e422expol = e422*expol;
    e412expol = e412*expol;
    %
    e411expol = node_values(e411expol', elements, nel);
    e422expol = node_values(e422expol', elements, nel);
    e412expol = node_values(e412expol', elements, nel);
    %
    s_extra = [e411expol; e422expol; e412expol];
    s_extra = C*s_extra;
end