function forces = traction_forces(traction_force, nodes, tnodes)

% ======================================================================
%
% Calculation of traction forces
%
%    output: 
%      forces - vector of traction forces
%                  
%    input:
%      traction_force - intensity of traction force                       
%      nodes - coordinates of nodes 
%      tnodes - numbers of traction forces nodes
%
% =========================================================================

    nnodes=size(nodes,1);
    tnodes_coord=nodes(tnodes,1:2);
    [~,order] = sortrows(sortrows(tnodes_coord,1),2);
    tnodes=tnodes(order);

    ntnodes=length(tnodes);
    %
    lines=[tnodes(1:ntnodes-1) tnodes(2:ntnodes)];
    lines_xy=nodes(lines(:,2),:)-nodes(lines(:,1),:);
    lines_length=sqrt(sum(lines_xy.^2,2));

    % assembly
    lines_force_x = lines_length*traction_force(1)/2;
    lines_force_y = lines_length*traction_force(2)/2;

    forces=full(sparse(lines(:,1)*2-1,1,lines_force_x,nnodes*2,1) + ... 
            sparse(lines(:,2)*2-1,1,lines_force_x,nnodes*2,1)+ ...
            sparse(lines(:,1)*2,1,lines_force_y,nnodes*2,1) + ... 
            sparse(lines(:,2)*2,1,lines_force_y,nnodes*2,1));
end