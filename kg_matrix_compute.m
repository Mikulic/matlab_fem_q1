function [KGTOTAL] = kg_matrix_compute(KELTOTAL, elements, ndof)

% ======================================================================
%
% Global stifness matrix KGTOTAL
%
%    output: 
%      KGTOTAL - global stifness matrix
%                  
%    input:
%      KELTOTAL - local stifness matrix for all alements                        
%      elements   - element nodes numbers 
%      ndof - degrees of freedom number
%
% =========================================================================
   
    KGTOTAL=sparse(ndof,ndof);
    DIAG=sparse(ndof,1);
    inodes=[1 1 2 2 3 3 4 4]; icomps=[1 2 1 2 1 2 1 2];
    
    % under-diagonal entries
    for i=1:8
        k=sparse(ndof,ndof);
        ik=2*(inodes(i)-1)+icomps(i); it=2*(elements(:,inodes(i))-1)+icomps(i);
        for j=1:i-1
            jl=2*(inodes(j)-1)+icomps(j); jt=2*(elements(:,inodes(j))-1)+icomps(j);
            k=k+sparse(it,jt,KELTOTAL{i,j},ndof,ndof);
        end
        DIAG=DIAG+sparse(it,1,KELTOTAL{ik,ik},ndof,1);
        KGTOTAL=KGTOTAL+k;
        clear k;
    end
    % transpose
    KGTOTAL=KGTOTAL+KGTOTAL.'+diag(DIAG);
end