function [C] = c_matrix_compute()

% ======================================================================
%
% Constitutive matrix computation
%
%    output: 
%      C - constitutive matrix
%                  
% =========================================================================

    EY = 210000;                      % Youngs modulus
    nu = 0.3;                         % Poisson's ratio
    C = (EY/(1-nu^2))*[1 nu 0 ; nu 1 0; 0 0 (1-nu)/2];
end