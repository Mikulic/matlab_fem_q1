function [B, detJ] = b_matrix_compute(xi, eta, xx, yy, nel)

% ======================================================================
%
% Strain displacement matrix B
%
%    output: 
%      B - strain displacement matrix at integration point (xi, eta)
%      detJ - determinant of Jacobian
%                  
%    input:
%      xi, eta  - coordinates of Gauss integration point
%      xx, yy   - coordinates of elements nodes                         
%      nel - number of elements
%
% =========================================================================

    Nd_xi = [eta/4 - 1/4, 1/4 - eta/4, eta/4 + 1/4, - eta/4 - 1/4];
    Nd_eta = [xi/4 - 1/4, - xi/4 - 1/4, xi/4 + 1/4, 1/4 - xi/4];
    
    J1=cell(2,2); [J1{:,:}]=deal(sparse(nel,1));
    J1{1,1} = xx*Nd_xi';
    J1{2,1} = xx*Nd_eta';
    J1{1,2} = yy*Nd_xi';
    J1{2,2} = yy*Nd_eta';
    
    detJ =J1{1,1}.*J1{2,2} - J1{1,2}.*J1{2,1};
    
    tInvJ1=cell(2,2); [tInvJ1{:,:}]=deal(sparse(nel,1));
    
    tInvJ1{1,1} = J1{2,2}./detJ;
    tInvJ1{1,2} = -J1{1,2}./detJ;
    tInvJ1{2,1} = -J1{2,1}./detJ;
    tInvJ1{2,2} = J1{1,1}./detJ;
    
    B3=zeros(4,8);
    B3(1:2,1:2:end)=[Nd_xi; Nd_eta];
    B3(3:4,2:2:end)=[Nd_xi; Nd_eta];

    B=cell(3,8); [B{:,:}]=deal(sparse(nel,1));
    
    B{1,1} = tInvJ1{1,1}*B3(1,1) + tInvJ1{1,2}*B3(2,1);
    B{1,3} = tInvJ1{1,1}*B3(1,3) + tInvJ1{1,2}*B3(2,3);
    B{1,5} = tInvJ1{1,1}*B3(1,5) + tInvJ1{1,2}*B3(2,5);
    B{1,7} = tInvJ1{1,1}*B3(1,7) + tInvJ1{1,2}*B3(2,7);
    
    B{2,2} = tInvJ1{2,1}*B3(3,2) + tInvJ1{2,2}*B3(4,2);
    B{2,4} = tInvJ1{2,1}*B3(3,4) + tInvJ1{2,2}*B3(4,4);
    B{2,6} = tInvJ1{2,1}*B3(3,6) + tInvJ1{2,2}*B3(4,6);
    B{2,8} = tInvJ1{2,1}*B3(3,8) + tInvJ1{2,2}*B3(4,8);
    
    B{3,1} = tInvJ1{2,1}*B3(1,1) + tInvJ1{2,2}*B3(2,1);
    B{3,2} = tInvJ1{1,1}*B3(3,2) + tInvJ1{1,2}*B3(4,2);
    B{3,3} = tInvJ1{2,1}*B3(1,3) + tInvJ1{2,2}*B3(2,3);
    B{3,4} = tInvJ1{1,1}*B3(3,4) + tInvJ1{1,2}*B3(4,4);
    B{3,5} = tInvJ1{2,1}*B3(1,5) + tInvJ1{2,2}*B3(2,5);
    B{3,6} = tInvJ1{1,1}*B3(3,6) + tInvJ1{1,2}*B3(4,6);
    B{3,7} = tInvJ1{2,1}*B3(1,7) + tInvJ1{2,2}*B3(2,7);
    B{3,8} = tInvJ1{1,1}*B3(3,8) + tInvJ1{1,2}*B3(4,8);

end
